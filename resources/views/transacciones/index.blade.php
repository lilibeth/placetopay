
@extends('welcome')


@section('style_extra')
    {{ Html::style('css/plugins/dataTables.bootstrap.css') }}
    {{ Html::style('css/plugins/sweetalert.css') }}
@show

@section('content')
    <div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">Historial de transacciones</h2>
    </div>
</div>
<div class="col-lg-12">
    <div class="btn-create">
        <a href="{{url('/transaccion/create')}}">
            <button id="bCreate" type="button" class="btn btn-success btn-circle">
                <i class="fa fa-plus"></i> Nueva transacción
            </button>
        </a>
    </div>
    <br>
    <br>
    <br>
    <div class="clearfix"></div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div id="table">
                <table class="table table-stripped">
                    <thead>
                        <tr>
                            <th>TransactionID</th>
                            <th>Return Code</th>
                            <th style="width: 200px">Response Reason Text</th>
                            <th>Reference</th>
                            <th>Transaction State</th>
                            <th>Bank Process Date</th>
                            <th>Estado</th>
                            <th>Fecha</th>
                            <th>Request</th>
                            <th>Response</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($trasacciones as $transaccion)
                        <tr>
                            <td>{{$transaccion->transactionID}}</td>
                            <td>{{$transaccion->returnCode}}</td>
                            <td>{{$transaccion->responseReasonText}}</td>
                            <td>{{$transaccion->reference}}</td>
                            <td>{{$transaccion->transactionState}}</td>
                            <td>{{$transaccion->bankProcessDate}}</td>
                            <td>{{$transaccion->type}}</td>
                            <td>{{$transaccion->created_at}}</td>
                            <td>
                                <div id='request_{{$transaccion->id}}' style='display:none'><pre>{{$transaccion->xml_request}}</pre></div>
                                <buttton class='btn btn-primary' onclick="swal({   
                                        title: 'Request!',   
                                        text: $('#request_{{$transaccion->id}}').html(),   
                                        html: true 
                                        });">Ver request</button>
                            </td>
                            <td>
                                <div id='response_{{$transaccion->id}}' style='display:none'><pre>{{$transaccion->xml_response}}</pre></div>
                                <buttton class='btn btn-success' onclick="swal({   
                                        title: 'Response!',   
                                        text: $('#response_{{$transaccion->id}}').html(),   
                                        html: true 
                                        });">Ver response</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('script_extra')
    <script>
        $(document).ready(function(){
            $("#table table").dataTable({
                aaSorting: []
            });
        });
    </script>
@stop