<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        @section('style_extra')
            {{ Html::style('css/bootstrap.css') }}
            <style>
                html, body {
                    background-color: #fff;
                    color: #636b6f;
                    font-family: 'Raleway', sans-serif;
                    font-weight: 100;
                    height: 100vh;
                    margin: 0;
                    
                }

                .full-height {
                    height: 100vh;
                }

                .flex-center {
                    align-items: center;                    
                    justify-content: center;
                }

                .position-ref {
                    position: relative;
                }

                .top-right {
                    position: absolute;
                    right: 10px;
                    top: 18px;
                }

                .content {
                    text-align: center;
                }

                .title {
                    font-size: 84px;
                }

                .links > a {
                    color: #636b6f;
                    padding: 0 25px;
                    font-size: 12px;
                    font-weight: 600;
                    letter-spacing: .1rem;
                    text-decoration: none;
                    text-transform: uppercase;
                }

                .m-b-md {
                    margin-bottom: 30px;
                }
            </style>
        @show
        

        <!-- js -->
        {{ Html::script('js/jquery-1.11.0.js') }}
        {{ Html::script('js/plugins/dataTables/jquery.dataTables.js') }}
        {{ Html::script('js/plugins/dataTables/dataTables.bootstrap.js') }}
        {{ Html::script('js/plugins/sweetalert.min.js') }}
        @section('script_extra')
        @show
    </head>
    <body>
        <div class="flex-center position-ref full-height">
                               
                <div class="content">
                    @section('content')
                        <div class="title m-b-md">
                            PlaceToPay
                        </div>

                        <div class="links">
                            <a href="transaccion/create">Crear transacciones</a>
                            <a href="transaccion">Listar transacciones</a>                        
                        </div>
                    @show
                </div>
        </div>
    </body>
</html>
