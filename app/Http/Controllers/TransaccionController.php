<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TransaccionForm;
use App\Libraries\Pse;
use Session;
use Redirect;

/*
Clase que controla la consulta y creación de transacciones
*/


class TransaccionController extends Controller
{

	public function index(){
		$trasacciones = \App\Transaccion::orderBy('created_at', 'DESC')->get();
        return View('transacciones.index', ['trasacciones' => $trasacciones]);
	}
     
	/*
	   metodo que crea el formulario de crear una transaccion
	*/
     public function create()
	{
		
	    $banks = $this->get_bank_list();
	    $objPse = new Pse();
        $tipo_documentos = $objPse->get_tipos_documentos();
        $tipo_persona = $objPse->get_interfaces_banco();
        $data = [
        	'banks' => $banks,
        	'tipo_persona' => $tipo_persona,
        	'tipo_documentos' => $tipo_documentos
        ];
	    return view("transacciones.create", $data);
	}

    /*
       metodo que resibe los datos del formulario los valida y guarda trasanccion
    */
	public function store(TransaccionForm $transaccionForm)
	{
        $ip = $_SERVER['REMOTE_ADDR'];
        $agente = substr($_SERVER['HTTP_USER_AGENT'], 0, 254);
        $url_retorno = url('/terminar_pago');
        $objPse = new Pse();
        $resultado = $objPse->crear_transaccion(\Request::all(), $ip, $agente, $url_retorno);
        if ($resultado['result']) {
            $response = $resultado['response'];
            \Session::put('PSETransactionID', $response->createTransactionResult->transactionID);
            if (isset($response->createTransactionResult->returnCode) && $response->createTransactionResult->returnCode == 'SUCCESS') {
                $this->guardar_respuesta($response->createTransactionResult, 'create', $resultado['xml_request'], $resultado['xml_response']);
                return \Redirect::to($response->createTransactionResult->bankURL);
            } else {
                $mensaje = 'La transacción no se realizó correctamente.';
            }
        } else {
            $mensaje = $resultado['message'];
        }
        return \Redirect::to('transaccion/create')->with('message', $mensaje);	  
	}
    
    /*
      metodo que termina la transaccion
    */

	public function terminar_pago()
	{
        $trasanccion_id = Session::get('PSETransactionID');
        
        if (empty($trasanccion_id)) {
            $mensaje = 'No se tiene la información necesaria para terminar la acción';
            return View::make('endPayment', compact('message'));
        }
        $objPse = new Pse();
        $resultado = $objPse->get_informacion_transaccion($trasanccion_id);
        if (!$resultado['result']) {
            $mensaje = $resultado['message'];
            return View::make('endPayment', compact('message'));
        }
        Session::forget('PSETransactionID');
        $informacion = $resultado['response']->getTransactionInformationResult;
        $this->guardar_respuesta($informacion, 'finish', $resultado['xml_request'], $resultado['xml_response']);
        switch ($informacion->transactionState) {
            case 'OK':
                $mensaje = 'La transacción se realizó satisfactoriamente';
                break;
            case 'NOT_AUTHORIZED':
                $mensaje = 'La transacción no ha sido autorizada, motivo: ' . $informacion->responseReasonText;
                break;
            case 'PENDING':
                $mensaje = 'La transacción se encuentra pendiente, motivo: ' . $informacion->responseReasonText;
                break;
            case 'FAILED':
                $mensaje = 'La transacción falló, motivo: ' . $informacion->responseReasonText;
                break;
        }
        $data =  ['message' => $mensaje];
        return View('transacciones.result_transaccion', $data);
	}


	/*
     * Método para consultar los bancos de la BD
     * los bancos se actualizan una vez al día por
     * medio del web service
     */
    private function get_bank_list() {
        $last_update = \App\Bank::get()->max('updated_at');
        $toDay = date('Y-m-d 00:00:00');
        if (empty($last_update) || $last_update < $toDay) {
            $objPse = new Pse();
            $arrBaks = $objPse->get_lista_bancos();
            if (!empty($arrBaks) && isset($arrBaks->getBankListResult->item)) {
                \App\Bank::whereNotNull('id')->delete();
                foreach ($arrBaks->getBankListResult->item as $bank) {
                    $newBank = new \App\Bank();
                    $newBank->bank_code = $bank->bankCode;
                    $newBank->bank_name = $bank->bankName;
                    $newBank->save();
                }
            }
        }

        $banks = \App\Bank::all()->pluck('bank_name', 'bank_code');
        return $banks;
    }


    /*
     * Guarda la respuesta de la transaacción en base de datos
     */
    private function guardar_respuesta($response, $type, $xmlRequest, $xmlResponse) {
        $transaccion = new \App\Transaccion();
        $transaccion->type = $type;
        $transaccion->xml_request = $xmlRequest;
        $transaccion->xml_response = $xmlResponse;
        foreach ($response as $key => $value) {
            $transaccion->$key = $value;
        }
        return $transaccion->save();
    }
}
