<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransaccionForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
     {
     return true;
     }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
             "bankCode"    =>    "required|min:2|max:4",             
             "bankInterface"    =>    "required|min:1|max:1",
             "payer.documentType"    =>    "required|min:2|max:3",
             "payer.document"    =>    "required|min:1|max:12",
             "payer.firstName"    =>    "required|min:2|max:60",
             "payer.lastName"    =>    "required|min:2|max:60",
             "payer.company"    =>    "required|min:2|max:60",
             "payer.emailAddress"    =>    "required|min:2|max:80|email",
             "payer.address"    =>    "required|min:2|max:100",
             "payer.city"    =>    "required|min:2|max:50",
             "payer.province"    =>    "required|min:2|max:50",
             "payer.phone"    =>    "required|min:2|max:30",
             "payer.mobile"    =>    "required|min:2|max:30",

        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [         
         'bankCode.required' => 'El campo banco es requerido!',
         'bankCode.min' => 'Elija un banco',
         'bankCode.max' => 'El campo bano no puede tener más de 4 carácteres',
         

         'bankInterface.required' => 'El campo Interface bancaria es requerido!',
         'bankInterface.min' => 'El campo Interface bancaria no puede tener menos de 1 carácteres',
         'bankInterface.max' => 'El campo Interface bancaria no puede tener más de 1 carácteres',
         'payer.documentType.required' => 'El campo tipo de documento es requerido!',
         'payer.documentType.min' => 'El campo tipo de documento no puede tener menos de 1 carácteres',
         'payer.documentType.max' => 'El campo tipo de documento no puede tener más de 3 carácteres',
         'payer.document.required' => 'El campo documento es requerido!',
         'payer.document.min' => 'El campo documento no puede tener menos de 1 carácteres',
         'payer.document.max' => 'El campo documento no puede tener más de 12 carácteres',

         'payer.firstName.required' => 'El campo  nombres es requerido!',
         'payer.firstName.min' => 'El campo  nombres no puede tener menos de 2 carácteres',
         'payer.firstName.max' => 'El campo  nombres  no puede tener más de 60 carácteres',

         'payer.lastName.required' => 'El campo  apellidos es requerido!',
         'payer.lastName.min' => 'El campo apellidos no puede tener menos de 2 carácteres',
         'payer.lastName.max' => 'El campo apellidos no puede tener más de 60 carácteres',

         'payer.company.required' => 'El campo  compañia es requerido!',
         'payer.company.min' => 'El campo compañia no puede tener menos de 2 carácteres',
         'payer.company.max' => 'El campo compañia no puede tener más de 60 carácteres',

         'payer.emailAddress.required' => 'El campo  email es requerido!',
         'payer.emailAddress.min' => 'El campo email no puede tener menos de 2 carácteres',
         'payer.emailAddress.max' => 'El campo email no puede tener más de 80 carácteres',
         'payer.emailAddress.email' => 'El campo email no tiene formato correcto',


         'payer.address.required' => 'El campo  direccion es requerido!',
         'payer.address.min' => 'El campo direccion no puede tener menos de 2 carácteres',
         'payer.address.max' => 'El campo direccion no puede tener más de 100 carácteres',

         'payer.city.required' => 'El campo  ciudad es requerido!',
         'payer.city.min' => 'El campo ciudad no puede tener menos de 2 carácteres',
         'payer.city.max' => 'El campo ciudad no puede tener más de 50 carácteres',

         'payer.province.required' => 'El campo departamento es requerido!',
         'payer.province.min' => 'El campo departamento no puede tener menos de 2 carácteres',
         'payer.province.max' => 'El campo departamento no puede tener más de 50 carácteres',

         'payer.phone.required' => 'El campo telefono es requerido!',
         'payer.phone.min' => 'El campo telefono no puede tener menos de 2 carácteres',
         'payer.phone.max' => 'El campo telefono no puede tener más de 30 carácteres',


         'payer.mobile.required' => 'El campo celular es requerido!',
         'payer.mobile.min' => 'El campo celular no puede tener menos de 2 carácteres',
         'payer.mobile.max' => 'El campo celular no puede tener más de 30 carácteres',

        ];
    }
}
