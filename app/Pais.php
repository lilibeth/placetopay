<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = 'paises';
 
	protected $fillable = [
		'nombre', 'iso_3166_1_a2',
		'iso_3166_1_a3', 'iso_3166_1_numeric'

	];
 
    protected $guarded = ['id'];
}
