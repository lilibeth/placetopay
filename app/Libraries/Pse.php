<?php  namespace App\Libraries;
use SoapClient;

/*
   Linbrería para consumir los web services de PSE.
*/


class Pse {

    private $wsdl = null;
    private $tranKey = null;

    public function __construct() {
        $this->wsdl = env('WSDL_PLACE_TO_PAY');
        $this->tranKey = env('TRANKEY_PLACE_TO_PAY');
    }


    /*
     * Arma la información necesario para 
     * crear una nueva transacción
     */
    
    private function crear_parametros_transaccion($data, $ip, $agent, $returnUrl) {
        $transaction = array(
            'bankCode' => $data['bankCode'],
            'bankInterface' => $data['bankInterface'],
            'returnURL' => $returnUrl,
            'reference' => md5(date('YmdHis')),
            'description' => 'Prueba pago',
            'language' => 'ES',
            'currency' => 'COP',
            'totalAmount' => 100,
            'taxAmount' => 0,
            'devolutionBase' => 0,
            'tipAmount' => 0,
            'payer' => $this->get_pagador($data['payer']),
            'buyer' => $this->get_compador(),
            'shipping' => $this->get_envio(),
            'ipAddress' => $ip,
            'userAgent' => $agent,
        );
        $autorizacion = $this->getAuth();
        return array(
            'transaction' => $transaction,
            'auth' => $autorizacion
        );
    }

    /*
     * Crea una transacción en PSE para iniciar el proceso de pago
     */
    public function crear_transaccion($data, $ip, $agent, $returnUrl){
        $parametros = $this->crear_parametros_transaccion($data, $ip, $agent, $returnUrl);
        $cliente = new SoapClient($this->wsdl, array ( 
                "location" => $this->wsdl));
        $result = array(
            'result' => false,
        );
        try{
            $result['response'] = $cliente->createTransaction($parametros);
            $result['xml_request'] = htmlentities(str_ireplace('><', ">\n<", $cliente->__getLastRequest()));
            $result['xml_response'] = htmlentities(str_ireplace('><', ">\n<", $cliente->__getLastResponse()));
            $result['result'] = true;
        } catch (Exception $ex) {
            $result['message'] = $ex->getMessage();
        }
        return $result;
    }


    /*
     * Arma la información necesaria para la autenticación
     * en el web service
     */

    private function getAuth() {
        $seed = date('c');
        $hash = sha1($seed . $this->tranKey, false);
        $arrCredentials = array(
            'login' => env('LOGIN_PLACE_TO_PAY', '6dd490faf9cb87a9862245da41170ff2'),
            'tranKey' => $hash,
            'seed' => $seed
        );
        return (object) $arrCredentials;
    }


    /*
     * Devuelve la información del comprador
     */
    private function get_compador() {
        $comprador = array(
            'documentType' => 'CC',
            'document' => '1064997608',
            'firstName' => 'lilibeth',
            'lastName' => 'fernandez fuentes',
            'company' => 'Prueba',
            'emailAddress' => 'liliferf@gmail.com',
            'address' => 'CL 23',
            'city' => 'Medellín',
            'province' => 'Antioquia',
            'country' => 'CO',
            'phone' => '2356545',
            'mobile' => '3005012133'
        );
        return $comprador;
    }
    /*
     * Devuelve la información de quien envía
     */

    private function get_envio() {
        $envio = array(
            'documentType' => 'CC',
            'document' => '1064997608',
            'firstName' => 'lilibeth',
            'lastName' => 'fernandez fuentes',
            'company' => 'Prueba',
            'emailAddress' => 'liliferf@gmail.com',
            'address' => 'CL 23',
            'city' => 'Medellín',
            'province' => 'Antioquia',
            'country' => 'CO',
            'phone' => '2356545',
            'mobile' => '3005012133'
        );
        return $envio;
    }


    /*
     * Consulta la información de una transacción usando
     * el transactionID devuelto en la respuesta de
     * crear transaccion
     */
    public function get_informacion_transaccion($transactionID){
        $client = new SoapClient($this->wsdl, array ( 
                "location" => $this->wsdl));
        $result = array(
            'result' => false,
        );
        try {
            $result['response'] = $client->getTransactionInformation(array('auth' => $this->getAuth(), 'transactionID' => $transactionID));
            $result['xml_request'] = htmlentities(str_ireplace('><', ">\n<", $client->__getLastRequest()));
            $result['xml_response'] = htmlentities(str_ireplace('><', ">\n<", $client->__getLastResponse()));
            $result['result'] = true;
        } catch (Exception $ex) {
            $result['message'] = $ex->getMessage();
        }
        return $result;
    }

    /*
     * Consulta la lista de bancos
     */
    public function get_lista_bancos() {
        $cliente = new SoapClient($this->wsdl, array ( 
                "location" => $this->wsdl));
        
        try {
            $array_banks = $cliente->getBankList(array('auth' => $this->getAuth()));
            
        } catch (Exception $ex) {
            $array_banks = array();
        }
        return $array_banks;
    }


     
    

    /*
     * Devuelve las interfaces bancarias disponibles
     */
    public function get_interfaces_banco(){
        return array(0 => 'Persona', 1 => 'Empresa');
    }
    

    /*
     * Devuelve la información del pagador
     */
    private function get_pagador($data) {
        $pagador = $data;
        $pagador['country'] = 'CO';
        return $pagador;
    }


    /*
     * Devuelve un listado de tipos de documentos disponibles
     */
    public function get_tipos_documentos(){
        return array(
                'CC' => 'Cédula de ciudanía colombiana', 
                'CE' => 'Cédula de extranjería',
                'TI' => 'Tarjeta de identidad',
                'PPN' => 'Pasaporte',
                'NIT' => 'Número de identificación tributaria',
                'SSN' => 'Social Security Number'
            );
    }
    
    
    
    

    

    
    
    

}
