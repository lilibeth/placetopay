-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 02-04-2018 a las 14:07:48
-- Versión del servidor: 5.7.21-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.28-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `placetopay`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banks`
--

CREATE TABLE `banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `banks`
--

INSERT INTO `banks` (`id`, `bank_name`, `bank_code`, `updated_at`, `created_at`) VALUES
(75, 'A continuación seleccione su banco', '0', '2018-04-02', '2018-04-02'),
(76, 'BAN.CO', '1552', '2018-04-02', '2018-04-02'),
(77, 'BANCO AGRARIO', '1040', '2018-04-02', '2018-04-02'),
(78, 'BANCO AGRARIO DESARROLLO', '1081', '2018-04-02', '2018-04-02'),
(79, 'BANCO AGRARIO QA DEFECTOS', '1080', '2018-04-02', '2018-04-02'),
(80, 'BANCO CAJA SOCIAL', '10322', '2018-04-02', '2018-04-02'),
(81, 'BANCO CAJA SOCIAL DESARROLLO', '1032', '2018-04-02', '2018-04-02'),
(82, 'BANCO COLPATRIA DESARROLLO', '1019', '2018-04-02', '2018-04-02'),
(83, 'BANCO COLPATRIA UAT', '1078', '2018-04-02', '2018-04-02'),
(84, 'BANCO COMERCIAL AVVILLAS S.A.', '1052', '2018-04-02', '2018-04-02'),
(85, 'BANCO COOMEVA S.A. - BANCOOMEVA', '1061', '2018-04-02', '2018-04-02'),
(86, 'BANCO COOPERATIVO COOPCENTRAL', '1016', '2018-04-02', '2018-04-02'),
(87, 'BANCO DAVIVIENDA', '1051', '2018-04-02', '2018-04-02'),
(88, 'BANCO DAVIVIENDA Desarrollo', '10512', '2018-04-02', '2018-04-02'),
(89, 'BANCO DE BOGOTA DESARROLLO 2013', '1001', '2018-04-02', '2018-04-02'),
(90, 'BANCO DE OCCIDENTE', '1023', '2018-04-02', '2018-04-02'),
(91, 'BANCO FALABELLA', '1062', '2018-04-02', '2018-04-02'),
(92, 'BANCO GNB COLOMBIA (ANTES HSBC)', '1010', '2018-04-02', '2018-04-02'),
(93, 'BANCO GNB SUDAMERIS', '1012', '2018-04-02', '2018-04-02'),
(94, 'BANCO PICHINCHA S.A.', '1060', '2018-04-02', '2018-04-02'),
(95, 'BANCO POPULAR', '1002', '2018-04-02', '2018-04-02'),
(96, 'BANCO PROCREDIT COLOMBIA', '1058', '2018-04-02', '2018-04-02'),
(97, 'Banco PSE', '1101', '2018-04-02', '2018-04-02'),
(98, 'BANCO SANTANDER COLOMBIA', '1065', '2018-04-02', '2018-04-02'),
(99, 'BANCO TEQUENDAMA', '1035', '2018-04-02', '2018-04-02'),
(100, 'Banco union Colombia Credito', '1004', '2018-04-02', '2018-04-02'),
(101, 'Banco union Colombia credito FD', '1005', '2018-04-02', '2018-04-02'),
(102, 'BANCO UNION COLOMBIANO', '1022', '2018-04-02', '2018-04-02'),
(103, 'Banco Web Service ACH WSE 3.0', '1055', '2018-04-02', '2018-04-02'),
(104, 'BANCOLOMBIA DATAPOWER', '10072', '2018-04-02', '2018-04-02'),
(105, 'BANCOLOMBIA DESARROLLO', '10071', '2018-04-02', '2018-04-02'),
(106, 'BANCOLOMBIA QA', '1007', '2018-04-02', '2018-04-02'),
(107, 'BBVA COLOMBIA S.A.', '1013', '2018-04-02', '2018-04-02'),
(108, 'CITIBANK COLOMBIA S.A.', '1009', '2018-04-02', '2018-04-02'),
(109, 'ITAU', '1006', '2018-04-02', '2018-04-02'),
(110, 'NEQUI CERTIFICACION', '1508', '2018-04-02', '2018-04-02'),
(111, 'Prueba Steve', '121212', '2018-04-02', '2018-04-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_29_03_200000_create_clientes_table', 2),
(4, '2018_29_03_300000_create_ciudades_table', 2),
(5, '2018_29_03_400000_create_departamentos_table', 2),
(6, '2018_29_03_500000_create_paises_table', 3),
(7, '2018_29_03_600000_create_transacciones_table', 4),
(8, '2018_30_03_700000_create_banks_table', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transacciones`
--

CREATE TABLE `transacciones` (
  `id` int(11) NOT NULL,
  `bankURL` varchar(255) DEFAULT NULL,
  `trazabilityCode` varchar(30) NOT NULL,
  `transactionCycle` int(11) NOT NULL,
  `transactionID` int(11) NOT NULL,
  `sessionID` varchar(32) DEFAULT NULL,
  `bankCurrency` varchar(3) DEFAULT NULL,
  `bankFactor` float DEFAULT NULL,
  `responseCode` int(11) NOT NULL,
  `responseReasonCode` varchar(3) NOT NULL,
  `responseReasonText` varchar(255) NOT NULL,
  `returnCode` varchar(30) NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `type` varchar(30) NOT NULL,
  `transactionState` varchar(30) DEFAULT NULL,
  `requestDate` varchar(255) DEFAULT NULL,
  `bankProcessDate` varchar(255) DEFAULT NULL,
  `onTest` int(11) DEFAULT NULL,
  `xml_request` text,
  `xml_response` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `transacciones`
--

INSERT INTO `transacciones` (`id`, `bankURL`, `trazabilityCode`, `transactionCycle`, `transactionID`, `sessionID`, `bankCurrency`, `bankFactor`, `responseCode`, `responseReasonCode`, `responseReasonText`, `returnCode`, `reference`, `type`, `transactionState`, `requestDate`, `bankProcessDate`, `onTest`, `xml_request`, `xml_response`, `created_at`, `updated_at`) VALUES
(1, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuzz88k2N%2fOUk%2bBOLoKRoOef6TXBReFh3v9FiW9QSgdCy', '1407605', 3, 1456491666, 'bf21342c79da6a2bc279abd6e243b6a8', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-03-31 15:53:39', '2018-03-31 15:53:39'),
(2, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuzz88k2N%2fOUk%2bBOLoKRoOee3Ha2JKVZT4HNNttIuaB0S', '1407606', 3, 1456491866, '6780b5ebd7a48a98b334abcb18174336', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-03-31 16:02:01', '2018-03-31 16:02:01'),
(3, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuzz88k2N%2fOUk%2bBOLoKRoOefQLHCKnCjSAHt18KWNgq3V', '1407607', 3, 1456492012, '4634c90c6d41185222a2d9c98d8b3df7', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-03-31 16:08:07', '2018-03-31 16:08:07'),
(4, NULL, '1194440', -1, 1442838501, 'f418c61e529ccf6a85a4f54efbcacea7', NULL, NULL, 1, '00', 'Aprobada', 'SUCCESS', '509aae71bd32f1104d4788d4e8acaaa0', 'finish', 'OK', '2016-10-24T13:24:52-05:00', '2016-10-24T13:25:41-05:00', 1, '', '', '2018-03-31 17:22:13', '2018-03-31 17:22:13'),
(5, NULL, '1194440', -1, 1442838501, 'f418c61e529ccf6a85a4f54efbcacea7', NULL, NULL, 1, '00', 'Aprobada', 'SUCCESS', '509aae71bd32f1104d4788d4e8acaaa0', 'finish', 'OK', '2016-10-24T13:24:52-05:00', '2016-10-24T13:25:41-05:00', 1, '', '', '2018-03-31 17:22:50', '2018-03-31 17:22:50'),
(6, NULL, '1194440', -1, 1442838501, 'f418c61e529ccf6a85a4f54efbcacea7', NULL, NULL, 1, '00', 'Aprobada', 'SUCCESS', '509aae71bd32f1104d4788d4e8acaaa0', 'finish', 'OK', '2016-10-24T13:24:52-05:00', '2016-10-24T13:25:41-05:00', 1, '', '', '2018-03-31 17:23:29', '2018-03-31 17:23:29'),
(7, NULL, '1194440', -1, 1442838501, 'f418c61e529ccf6a85a4f54efbcacea7', NULL, NULL, 1, '00', 'Aprobada', 'SUCCESS', '509aae71bd32f1104d4788d4e8acaaa0', 'finish', 'OK', '2016-10-24T13:24:52-05:00', '2016-10-24T13:25:41-05:00', 1, '', '', '2018-03-31 17:24:34', '2018-03-31 17:24:34'),
(8, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuzz88k2N%2fOUk%2bBOLoKRoOecNcSwWm%2bDfiQfYgDYSga2I', '1407631', 1, 1456505039, '3ae72ed99195b2556a333563ac19876b', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-01 13:44:59', '2018-04-01 13:44:59'),
(9, NULL, '1194440', -1, 1442838501, 'f418c61e529ccf6a85a4f54efbcacea7', NULL, NULL, 1, '00', 'Aprobada', 'SUCCESS', '509aae71bd32f1104d4788d4e8acaaa0', 'finish', 'OK', '2016-10-24T13:24:52-05:00', '2016-10-24T13:25:41-05:00', 1, '', '', '2018-04-01 13:47:26', '2018-04-01 13:47:26'),
(10, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuzz88k2N%2fOUk%2bBOLoKRoOedrIQjY0IV2MDfmMHwtCRy4', '1407632', 2, 1456505445, '953857afee8a92497d4ee44517fcf1e6', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-01 14:23:28', '2018-04-01 14:23:28'),
(11, NULL, '1194440', -1, 1442838501, 'f418c61e529ccf6a85a4f54efbcacea7', NULL, NULL, 1, '00', 'Aprobada', 'SUCCESS', '509aae71bd32f1104d4788d4e8acaaa0', 'finish', 'OK', '2016-10-24T13:24:52-05:00', '2016-10-24T13:25:41-05:00', 1, '', '', '2018-04-01 14:24:57', '2018-04-01 14:24:57'),
(12, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuzz88k2N%2fOUk%2bBOLoKRoOeeiVV5N6Utf7jlbtKpGmO%2bZ', '1407664', 5, 1456533821, '7e9950b0b07224a14a0f5c2ffdf02e50', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 01:21:25', '2018-04-02 01:21:25'),
(13, NULL, '1194440', -1, 1442838501, 'f418c61e529ccf6a85a4f54efbcacea7', NULL, NULL, 1, '00', 'Aprobada', 'SUCCESS', '509aae71bd32f1104d4788d4e8acaaa0', 'finish', 'OK', '2016-10-24T13:24:52-05:00', '2016-10-24T13:25:41-05:00', 1, '', '', '2018-04-02 01:22:48', '2018-04-02 01:22:48'),
(14, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuzz88k2N%2fOUk%2bBOLoKRoOedDYZH%2fCBTuldwevHKyDRQQ', '1407665', 5, 1456534049, 'defda1c44093e9c26aa7251352d767c7', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 01:35:57', '2018-04-02 01:35:57'),
(15, NULL, '1194440', -1, 1442838501, 'f418c61e529ccf6a85a4f54efbcacea7', NULL, NULL, 1, '00', 'Aprobada', 'SUCCESS', '509aae71bd32f1104d4788d4e8acaaa0', 'finish', 'OK', '2016-10-24T13:24:52-05:00', '2016-10-24T13:25:41-05:00', 1, '', '', '2018-04-02 01:36:22', '2018-04-02 01:36:22'),
(16, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuzz88k2N%2fOUk%2bBOLoKRoOed4iaUypU99%2f8uVSK5gIcxI', '1407666', 5, 1456534153, 'ed68679838a4fda01e58d9d846d80908', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 01:41:19', '2018-04-02 01:41:19'),
(17, NULL, '1194440', -1, 1442838501, 'f418c61e529ccf6a85a4f54efbcacea7', NULL, NULL, 1, '00', 'Aprobada', 'SUCCESS', '509aae71bd32f1104d4788d4e8acaaa0', 'finish', 'OK', '2016-10-24T13:24:52-05:00', '2016-10-24T13:25:41-05:00', 1, '', '', '2018-04-02 01:42:02', '2018-04-02 01:42:02'),
(18, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuzz88k2N%2fOUk%2bBOLoKRoOedpQ0FxhGGZXlwy4PoKWFfK', '1407669', 5, 1456535117, 'a342b86b0d6167cb8b1ee320b62c4902', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 02:41:05', '2018-04-02 02:41:05'),
(19, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbu2RjX4PCM3p9oJpsxToymIjPYVHi6aZAFUeuMJ2XZJ7k', '1407795', 3, 1456545375, '1483ec3625772abdeef95fd33de11adc', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 16:17:25', '2018-04-02 16:17:25'),
(20, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbu2RjX4PCM3p9oJpsxToymIhpSd5x0OAG3grg%2foWmKhT0', '1407796', 3, 1456545471, '40805962a7280f7f438c4b8e7d702786', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 16:19:27', '2018-04-02 16:19:27'),
(21, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuwkSzWENVJODhIvLPY5xcFAfcUMPdj7BhpqYzpsfUKal', '1407801', 3, 1456545646, 'a898c083bbddea7d0406e5fa7c5ff3a9', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 16:23:37', '2018-04-02 16:23:37'),
(22, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuwkSzWENVJODhIvLPY5xcFBC2i%2b2ip8kFLsWrmeTCFXS', '1407804', 3, 1456545794, 'a86334a39af3ddd197955c79007fe3e2', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 16:27:09', '2018-04-02 16:27:09'),
(23, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuwkSzWENVJODhIvLPY5xcFCFbNVi2vsKO7jvg6H6F0N%2b', '1407813', 3, 1456546489, 'c1a08a7437cf2a9327211f7a75794bce', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 16:42:10', '2018-04-02 16:42:10'),
(24, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuwkSzWENVJODhIvLPY5xcFA7jG4IXoXq80%2fGmd21lZFI', '1407814', 3, 1456546557, '0b324405e1988ca17944c032d1161db3', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 16:43:38', '2018-04-02 16:43:38'),
(25, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuwkSzWENVJODhIvLPY5xcFBS%2fFEAx7AK6Wb5DcJvUGQK', '1407833', 3, 1456547658, 'f92570ca4d0a0895746b7d20de93e3ca', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 17:10:16', '2018-04-02 17:10:16'),
(26, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuwkSzWENVJODhIvLPY5xcFDxQjl9Hqr6t7SjHMiA0chZ', '1407835', 3, 1456548113, '6bf0e1267d98d5f61ef45b904fbd4d38', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 17:24:01', '2018-04-02 17:24:01'),
(27, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuwkSzWENVJODhIvLPY5xcFDlNWlL9xkJZXHe5LTtYIf5', '1407839', 3, 1456548435, '953fc888ea40255f6876d76f311118c4', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 17:34:02', '2018-04-02 17:34:02'),
(28, NULL, '1194440', -1, 1442838501, 'f418c61e529ccf6a85a4f54efbcacea7', NULL, NULL, 1, '00', 'Aprobada', 'SUCCESS', '509aae71bd32f1104d4788d4e8acaaa0', 'finish', 'OK', '2016-10-24T13:24:52-05:00', '2016-10-24T13:25:41-05:00', 1, '', '', '2018-04-02 17:34:33', '2018-04-02 17:34:33'),
(29, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuwkSzWENVJODhIvLPY5xcFB8JxJcRl5kIecZp1uwoD6c', '1407841', 3, 1456548996, 'ea7125e710941d704c1a1e47aebe6373', 'COP', 1, 3, '?-', 'Transacción pendiente. Por favor verificar si el débito fue realizado en el Banco.', 'SUCCESS', NULL, 'create', NULL, NULL, NULL, NULL, '', '', '2018-04-02 17:51:13', '2018-04-02 17:51:13'),
(30, NULL, '1407841', 3, 1456548996, 'ea7125e710941d704c1a1e47aebe6373', NULL, NULL, 1, '00', 'Aprobada', 'SUCCESS', '0991225808f164374dd3396e3bc0afd8', 'finish', 'OK', '2018-04-02T12:51:12-05:00', '2018-04-02T12:51:43-05:00', 1, '', '', '2018-04-02 17:51:43', '2018-04-02 17:51:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
