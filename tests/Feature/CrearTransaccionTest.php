<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CrearTransaccionTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {

    	 $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
    	 $_SERVER['HTTP_USER_AGENT'] = '';
         $response = $this->visit('/transaccion/create')
         ->select('1022', 'bankCode')
         ->select('0', 'bankInterface')
         ->select('CC', 'payer[documentType]')
         ->type('1064997608', 'payer[document]')
         ->type('lilibeth', 'payer[firstName]')
         ->type('fernandez fuentes', 'payer[lastName]')
         ->type('sytty', 'payer[company]')
         ->type('liliferf@gmail.com', 'payer[emailAddress]')
         ->type('CR 48 # 46 - 87 Oficina 301', 'payer[address]')
         ->type('Cereté', 'payer[city]')
         ->type('Córdoba', 'payer[province]')
         ->type('323423', 'payer[phone]')
         ->type('3005012133', 'payer[mobile]')
         ->press('Enviar');

         
         
         $response->assertSee('https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm');



    }
}
