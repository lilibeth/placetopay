<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('PlaceToPay')
             ->dontSee('Rails');
    }


    public function testListarTransaccionesExample()
	{
	    $this->visit('/')
	         ->click('Listar transacciones')
	         ->seePageIs('/transaccion');
	}


	public function testcreaeTransaccionExample()
	{
	    $this->visit('/')
	         ->click('Crear transacciones')
	         ->seePageIs('/transaccion/create');
	}
}
